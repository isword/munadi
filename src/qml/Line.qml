import QtQuick 2.0

Rectangle {
    color: theme.borderColour
    height: 1
    opacity: 0.5
}
