import QtQuick 2.5

Item {

    property string borderColour: "darkgrey" //"#EDEDED"
    property string fontColour: "#545454"

    property int margin: mm.stickerMode ? 5 : 10
}
