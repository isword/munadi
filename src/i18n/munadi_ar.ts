<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar_EG">
<context>
    <name>AdjustmentComponent</name>
    <message>
        <location filename="../qml/AdjustmentComponent.qml" line="24"/>
        <source>Volume</source>
        <translation>الصوت</translation>
    </message>
    <message>
        <location filename="../qml/AdjustmentComponent.qml" line="40"/>
        <source>%1 %</source>
        <translation>%1 ٪</translation>
    </message>
    <message>
        <location filename="../qml/AdjustmentComponent.qml" line="64"/>
        <source>+ %1</source>
        <translation>+ %1</translation>
    </message>
    <message>
        <location filename="../qml/AdjustmentComponent.qml" line="65"/>
        <source>- %1</source>
        <translation>- %1</translation>
    </message>
    <message>
        <source> %</source>
        <translation type="vanished"> ٪</translation>
    </message>
    <message>
        <source>+ </source>
        <translation type="vanished">+ </translation>
    </message>
    <message>
        <source>- </source>
        <translation type="vanished">- </translation>
    </message>
    <message>
        <source>%</source>
        <translation type="vanished">٪</translation>
    </message>
    <message>
        <location filename="../qml/AdjustmentComponent.qml" line="44"/>
        <source>Minutes</source>
        <translation>دقائق</translation>
    </message>
</context>
<context>
    <name>AudioManComp</name>
    <message>
        <location filename="../qml/AudioManComp.qml" line="10"/>
        <source>Please choose an audio file</source>
        <translation>اختر ملفا صوتياً</translation>
    </message>
</context>
<context>
    <name>CompassPage</name>
    <message>
        <location filename="../qml/CompassPage.qml" line="16"/>
        <source>Qibla Direction</source>
        <translation>اتجاه القبلة</translation>
    </message>
    <message>
        <location filename="../qml/CompassPage.qml" line="92"/>
        <source>%1° with respect to North</source>
        <translation>%1° إلى الشمال</translation>
    </message>
</context>
<context>
    <name>LocationSearchComponent</name>
    <message>
        <location filename="../qml/LocationSearchComponent.qml" line="61"/>
        <source>Enter city name</source>
        <translation>أدخل اسم المدينة</translation>
    </message>
    <message>
        <location filename="../qml/LocationSearchComponent.qml" line="69"/>
        <source>Search</source>
        <translation>بحث</translation>
    </message>
    <message>
        <location filename="../qml/LocationSearchComponent.qml" line="159"/>
        <source>Location</source>
        <translation>الموقع</translation>
    </message>
    <message>
        <location filename="../qml/LocationSearchComponent.qml" line="171"/>
        <source>Timezone</source>
        <translation>المنطقة الزمنية</translation>
    </message>
    <message>
        <location filename="../qml/LocationSearchComponent.qml" line="183"/>
        <source>Latitude</source>
        <translation>خط عرض</translation>
    </message>
    <message>
        <location filename="../qml/LocationSearchComponent.qml" line="194"/>
        <source>Longitude</source>
        <translation>خط طول</translation>
    </message>
</context>
<context>
    <name>MunadiModel</name>
    <message>
        <location filename="../qml/MunadiModel.qml" line="108"/>
        <source>Makkah (built-in)</source>
        <translation>مكة (مدمج)</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="116"/>
        <source>Muslim World League</source>
        <translation>رابطة العالم الإسلامي</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="121"/>
        <source>Egyptian</source>
        <translation>مصري</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="126"/>
        <source>Karachi</source>
        <translation>كراتشي</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="131"/>
        <source>Umm AlQura</source>
        <translation>أم القرى</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="136"/>
        <source>Dubai</source>
        <translation>دبي</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="141"/>
        <source>Moonsighting Committee</source>
        <translation>لجنة الرؤية القمرية</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="146"/>
        <source>North America</source>
        <translation>أمريكا الشمالية</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="151"/>
        <source>Kuwait</source>
        <translation>الكويت</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="156"/>
        <source>Qatar</source>
        <translation>قطر</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="161"/>
        <source>Singapore</source>
        <translation>سنغافورة</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="166"/>
        <source>Turkey</source>
        <translation>تركيا</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="171"/>
        <source>Other</source>
        <translation>اُخرى</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="82"/>
        <source>Fajr</source>
        <translation>الفجر</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="82"/>
        <source>Sunrise</source>
        <translation>الشروق</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="82"/>
        <source>Dhuhr</source>
        <translation>الظهر</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="83"/>
        <source>Asr</source>
        <translation>العصر</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="83"/>
        <source>Maghrib</source>
        <translation>المغرب</translation>
    </message>
    <message>
        <location filename="../qml/MunadiModel.qml" line="83"/>
        <source>Isha</source>
        <translation>العشاء</translation>
    </message>
    <message>
        <source>Hijri</source>
        <translation type="vanished">هجري</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/SettingsPage.qml" line="46"/>
        <source>Application</source>
        <translation>تطبيق</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="57"/>
        <source>Show on Athan</source>
        <translation>أظهر عند الأذان</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="61"/>
        <source>Try to show window on Athan.</source>
        <translation>حاول إظهار النافذة عند الأذان.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="79"/>
        <source>Autostart</source>
        <translation>تشغيل آلي</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="83"/>
        <source>Try to start minimised after login.</source>
        <translation>حاول البدء مصغراً عند البداية.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="132"/>
        <source>Mute all Athans</source>
        <translation>أخفت أصوات الأذان</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="150"/>
        <source>Athan</source>
        <translation>الأذان</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="164"/>
        <source>Prayer</source>
        <translation>الصلاة</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="178"/>
        <source>Other</source>
        <translation>اُخرى</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="188"/>
        <source>Mathhab</source>
        <translation>المذهب</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="200"/>
        <source>Majority</source>
        <translation>الجمهور</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="203"/>
        <source>Hanafi</source>
        <translation>حنفي</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="210"/>
        <source>Algorithm</source>
        <translation>الخوارزمية</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="265"/>
        <source>+ %1</source>
        <translation>+ %1</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="267"/>
        <source>- %1</source>
        <translation>- %1</translation>
    </message>
    <message>
        <source>Muslim World League</source>
        <translation type="vanished">رابطة العالم الإسلامي</translation>
    </message>
    <message>
        <source>Egyptian</source>
        <translation type="vanished">مصري</translation>
    </message>
    <message>
        <source>Karachi</source>
        <translation type="vanished">كراتشي</translation>
    </message>
    <message>
        <source>Umm AlQura</source>
        <translation type="vanished">أم القرى</translation>
    </message>
    <message>
        <source>Dubai</source>
        <translation type="vanished">دبي</translation>
    </message>
    <message>
        <source>Moonsighting Committee</source>
        <translation type="vanished">لجنة الرؤية القمرية</translation>
    </message>
    <message>
        <source>North America</source>
        <translation type="vanished">أمريكا الشمالية</translation>
    </message>
    <message>
        <source>Kuwait</source>
        <translation type="vanished">الكويت</translation>
    </message>
    <message>
        <source>Qatar</source>
        <translation type="vanished">قطر</translation>
    </message>
    <message>
        <source>Singapore</source>
        <translation type="vanished">سنغافورة</translation>
    </message>
    <message>
        <source>Turkey</source>
        <translation type="vanished">تركيا</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="228"/>
        <source>Calendar</source>
        <translation>التقويم</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="242"/>
        <source>Hijri</source>
        <translation>هجري</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="245"/>
        <source>Gregorian</source>
        <translation>غريغوري</translation>
    </message>
    <message>
        <source>+ </source>
        <translation type="vanished">+ %1</translation>
    </message>
    <message>
        <source>- </source>
        <translation type="vanished">- </translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="290"/>
        <source>About Munadi</source>
        <translation>ما هو منادي</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="303"/>
        <source>Website</source>
        <translation>الموقع</translation>
    </message>
    <message>
        <location filename="../qml/SettingsPage.qml" line="315"/>
        <source>Version</source>
        <translation>الإصدار</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source> Athan ...</source>
        <translation type="vanished">أذان 1% ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="19"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>QGuiApplication</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="90"/>
        <source>%1 Athan ...</source>
        <translation>أذان %1 ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>h </source>
        <translation>س </translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>m until </source>
        <translation>د حتى </translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="121"/>
        <source>Fajr</source>
        <translation>الفجر</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="182"/>
        <source>Home</source>
        <translation>الرئيسية</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="185"/>
        <source>Settings</source>
        <translation>الإعدادات</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="252"/>
        <source>Quit</source>
        <translation>إنهاء</translation>
    </message>
</context>
</TS>
